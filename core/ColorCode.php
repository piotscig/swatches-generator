<?php

namespace piotrscigala;

class ColorCode
{
    protected string $colorHexCodeRGB;

    protected string $blackHexColorRGB;
    protected string $whiteHexColorRGB;

    protected float $contrastToBlack;
    protected float $contrastToWhite;

    protected string $contrastingTextColor;
    protected bool $contrastSufficient;

    public function __construct(
        string $colorCode,
        string $whiteHexColorRGB = "#FFFFFF",
        string $blackHexColorRGB = "#000000"
    )
    {
        $this->setColorHexCodeRGB(
            self::prependHashIfMissing($colorCode)
        );
        $this->setBlackHexColorRGB(
            self::prependHashIfMissing($blackHexColorRGB)
        );
        $this->setWhiteHexColorRGB(
            self::prependHashIfMissing($whiteHexColorRGB)
        );

        $this->setContrastToBlack(
            ColorContrastChecker::calculateLuminosityContrastRatio(
                $this->getColorHexCodeRGB(),
                $this->getblackHexColorRGB()
            )
        );

        $this->setContrastToWhite(
            ColorContrastChecker::calculateLuminosityContrastRatio(
                $this->getcolorHexCodeRGB(),
                $this->getwhiteHexColorRGB()
            )
        );

        $this->setContrastingTextColor(
            $this->getDeterminedContrastingColor()
        );

        $this->setContrastSufficient(
            ColorContrastChecker::isContrastRatioSufficient(
                ColorContrastChecker::calculateLuminosityContrastRatio(
                    $this->getColorHexCodeRGB(),
                    $this->getContrastingTextColor()
                )
            )
        );
    }

    /**
     * Determine whether black or white should be used as contrasting color and return color code accordingly
     *
     * @return string
     */
    protected function getDeterminedContrastingColor(): string
    {
        if (ColorContrastChecker::isContrastRatioSufficient(
            $this->getContrastToBlack()
        )) {
            return $this->getBlackHexColorRGB();
        } else {
            return $this->getWhiteHexColorRGB();
        }
    }

    /**
     * Checks whether given RGB hex color string misses initial hash (#). In such case, it prepends given string with it
     *
     * @param string $color
     * @return string
     */
    public static function prependHashIfMissing(string $color): string
    {
        if (!preg_match('/^[#]/', $color)) {
            return '#' . $color;
        }

        return $color;
    }

    /**
     * Verifies whether given color is a hex RGB color
     *
     * @param string $color
     * @return bool
     */
    public static function isHexColorRGB(string $color): bool
    {
        return preg_match('/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/', $color);
    }

    /**
     * @param string $colorHexCodeRGB
     */
    protected function setColorHexCodeRGB(string $colorHexCodeRGB): void
    {
        if ($this->isHexColorRGB($colorHexCodeRGB)) {
            $this->colorHexCodeRGB = $colorHexCodeRGB;
        } else {
            $this->colorHexCodeRGB = "#000000";
        }
    }

    /**
     * @param string $blackHexColorRGB
     */
    protected function setBlackHexColorRGB(string $blackHexColorRGB): void
    {
        if ($this->isHexColorRGB($blackHexColorRGB)) {
            $this->blackHexColorRGB = $blackHexColorRGB;
        } else {
            $this->blackHexColorRGB = "#000000";
        }
    }

    /**
     * @param string $whiteHexColorRGB
     */
    protected function setWhiteHexColorRGB(string $whiteHexColorRGB): void
    {
        if ($this->isHexColorRGB($whiteHexColorRGB)) {
            $this->whiteHexColorRGB = $whiteHexColorRGB;
        } else {
            $this->whiteHexColorRGB = "#FFFFFF";
        }
    }

    /**
     * @param string $contrastingTextColor
     */
    protected function setContrastingTextColor(string $contrastingTextColor): void
    {
        $this->contrastingTextColor = $contrastingTextColor;
    }

    /**
     * @param float $contrastToBlack
     */
    protected function setContrastToBlack(float $contrastToBlack): void
    {
        $this->contrastToBlack = $contrastToBlack;
    }

    /**
     * @param float $contrastToWhite
     */
    protected function setContrastToWhite(float $contrastToWhite): void
    {
        $this->contrastToWhite = $contrastToWhite;
    }

    /**
     * @param bool $contrastSufficient
     */
    protected function setContrastSufficient(bool $contrastSufficient): void
    {
        $this->contrastSufficient = $contrastSufficient;
    }

    /**
     * @return string
     */
    public function getColorHexCodeRGB(): string
    {
        return $this->colorHexCodeRGB;
    }

    /**
     * @return string
     */
    public function getBlackHexColorRGB(): string
    {
        return $this->blackHexColorRGB;
    }

    /**
     * @return string
     */
    public function getWhiteHexColorRGB(): string
    {
        return $this->whiteHexColorRGB;
    }

    /**
     * @return float
     */
    public function getContrastToBlack(): float
    {
        return $this->contrastToBlack;
    }

    /**
     * @return float
     */
    public function getContrastToWhite(): float
    {
        return $this->contrastToWhite;
    }

    /**
     * @return bool
     */
    public function isContrastSufficient(): bool
    {
        return $this->contrastSufficient;
    }

    /**
     * @return string
     */
    public function getContrastingTextColor(): string
    {
        return $this->contrastingTextColor;
    }
}
