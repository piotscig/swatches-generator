<?php

namespace piotrscigala;

class ColorContrastChecker
{
    public const MINIMUM_CONTRAST_RATIO = 5;

    /**
     * @param string $color1HexCodeRGB
     * @param string $color2HexCodeRGB
     * @return float
     */
    public static function calculateLuminosityContrastRatio(
        string $color1HexCodeRGB,
        string $color2HexCodeRGB
    ): float
    {
        $color1RedFactor = hexdec(
            substr($color1HexCodeRGB, 1, 2)
        );
        $color1GreenFactor = hexdec(
            substr($color1HexCodeRGB, 3, 2)
        );
        $color1BlueFactor = hexdec(
            substr($color1HexCodeRGB, 5, 2)
        );

        $color2RedFactor = hexdec(
            substr($color2HexCodeRGB, 1, 2)
        );
        $color2GreenFactor = hexdec(
            substr($color2HexCodeRGB, 3, 2)
        );
        $color2BlueFactor = hexdec(
            substr($color2HexCodeRGB, 5, 2)
        );

        $L1 = 0.2126 * pow($color1RedFactor / 255, 2.2) +
            0.7152 * pow($color1GreenFactor / 255, 2.2) +
            0.0722 * pow($color1BlueFactor / 255, 2.2);

        $L2 = 0.2126 * pow($color2RedFactor / 255, 2.2) +
            0.7152 * pow($color2GreenFactor / 255, 2.2) +
            0.0722 * pow($color2BlueFactor / 255, 2.2);

        if ($L1 > $L2) {
            $contrastRatio = (int)(($L1 + 0.05) / ($L2 + 0.05));
        } else {
            $contrastRatio = (int)(($L2 + 0.05) / ($L1 + 0.05));
        }

        return $contrastRatio;
    }

    /**
     * Check whether given contrast ratio is above minimum required level
     *
     * @param float $contrastRatio
     * @return bool
     */
    public static function isContrastRatioSufficient(float $contrastRatio): bool
    {
        return ($contrastRatio > self::MINIMUM_CONTRAST_RATIO);
    }

    /**
     * Returns inversed RGB color hex code
     *
     * @link https://www.jonasjohn.de/snippets/php/color-inverse.htm
     *
     * @param string $colorHexCodeRGB
     * @return string
     */
    protected static function getInversedColor(string $colorHexCodeRGB): string
    {
        $colorHexCodeRGB = str_replace('#', '', $colorHexCodeRGB);

        if (6 != strlen($colorHexCodeRGB)) {
            $colorHexCodeRGB = str_repeat(
                    substr($colorHexCodeRGB, 0, 1),
                    2
                )
                . str_repeat(
                    substr($colorHexCodeRGB, 1, 1),
                    2
                )
                . str_repeat(
                    substr($colorHexCodeRGB, 2, 1),
                    2
                );
        }

        $rgb = '';

        for ($x = 0; $x < 3; $x++) {
            $c = 255 - hexdec(substr($colorHexCodeRGB, (2 * $x), 2));
            $c = ($c < 0) ? 0 : dechex($c);
            $rgb .= (strlen($c) < 2) ? '0' . $c : $c;
        }

        return '#' . $rgb;
    }
}
