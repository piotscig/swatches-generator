<?php

namespace piotrscigala;

use URLify;

class ColorCodesCollection
{
    protected string $name = "";
    protected string $slug = "";
    protected array $codes = [];

    /**
     * ColorCodesCollection constructor.
     * @param string $name
     * @param string $slug
     */
    public function __construct(
        string $name,
        string $slug = ""
    )
    {
        $this->name = $name;
        $this->slug = $slug;
    }

    /**
     * @param string $colorCode
     */
    public function addColorCode(string $colorCode)
    {
        $this->codes[] = new ColorCode(
            $colorCode
        );
    }

    /**
     * @return bool
     */
    public function isColorCodesCountOdd(): bool
    {
        return 0 != $this->getColorCodesCount() % 2;
    }

    /**
     * @return int
     */
    public function getColorCodesCount(): int
    {
        return count($this->getColorCodes());
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        if ("" == $this->slug) {
            return URlify::filter ($this->name, 30, 'pl');
        } else {
            return $this->slug;
        }
    }

    /**
     * @return array
     */
    public function getColorCodes(): array
    {
        return $this->codes;
    }
}
