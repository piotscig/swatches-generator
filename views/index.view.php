<?php require('partials/head.php'); ?>

<h1>Swatches generator</h1>

<form action="/output" method="POST" enctype="multipart/form-data">
    <fieldset>
        <div>
            <label for="color-name">Name of the color:</label>
            <input type="text" id="color-name" name="color-name" required aria-required="true">
        </div>
        <div>
            <label for="color-slug">Slug (optional):</label>
            <input type="text" id="color-slug" name="color-slug" placeholder="color-slug">
        </div>
        <div>
            <label for="input-svg">Choose input SVG file:</label>
            <input type="file"
                   id="input-svg"
                   name="input-svg"
                   accept=".svg,.xml,image/svg+xml"
                   required
                   aria-required="true"
            >
        </div>
    </fieldset>
    <div>
        <button>Generate swatches</button>
    </div>
</form>

<?php require('partials/footer.php'); ?>
