<?php require('partials/head.php'); ?>

<h1>Swatches generator</h1>

<section>
    <h2>Output demo:</h2>
    <style>
        <?php require('partials/swatches-group-style.php'); ?>
    </style>

    <?php require('partials/swatches-group.php'); ?>
</section>

<section>
    <h2>Output code:</h2>
    <h3>HTML:</h3>
    <pre><code class="language-html"><?= $htmlCodeOutput ?></code></pre>
    <h3>CSS:</h3>
    <pre><code class="language-css"><?= $cssCodeOutput ?></code></pre>
</section>

<link rel="stylesheet"
      href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script>

<?php require('partials/footer.php'); ?>
