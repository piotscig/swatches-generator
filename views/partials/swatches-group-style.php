<style>
<?php
    for ($i = 0; $i < $colorCodesCount; $i++):
?>
.swatch--<?= $colorSlug ?>-<?= $i+1 ?> {
    background: <?= $colorCodes[$i] ?>;
    color: <?= $contrastingTextColorCodes[$i] ?>;
}
<?php
    endfor;
?>
</style>