<div class="swatches__group">
<?php
    for ($i = 0; $i < $colorCodesCount; $i++):
?>
    <div class="swatch swatch--<?= $colorSlug ?>-<?= $i+1 ?><?=
        $contrastsSufficient[$i] ? "" : " swatch--contrast-insufficient" ?>">
        <span class="swatch__color-code"><?= $colorCodes[$i] ?></span>
    </div>
<?php
    endfor;
?>
</div>