<!doctype html>

<html lang="pl-PL">
<head>
    <meta charset="utf-8">

    <link rel="preconnect" href="https://fonts.gstatic.com">

    <title>Gama kolorystyczna – generator próbek</title>
    <meta name="description" content="Generator próbek koloru do gamy kolorystycznej">
    <meta name="author" content="Piotr Ścigała">

    <link rel="stylesheet" href="/public/css/styles.css?v=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Zilla+Slab:wght@300;400;700&display=swap&subset=latin-ext"
          rel="stylesheet">
</head>
<body>