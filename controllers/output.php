<?php

use enshrined\svgSanitize\Sanitizer;

require_once 'core/ColorContrastChecker.php';
require_once 'core/ColorCode.php';
require_once 'core/ColorCodesCollection.php';

use piotrscigala\ColorCode;
use piotrscigala\ColorCodesCollection;

$errorMessage = "";

if (
    isset($_FILES['input-svg']['name'])
    &&
    !empty($_FILES['input-svg']['name'])
) {
    $size = $_FILES['input-svg']['size'];
    $type = $_FILES['input-svg']['type'];
    $tmp_name = $_FILES['input-svg']['tmp_name'];
    $error = $_FILES['input-svg']['error'];
    $maxsize = 2097152;

    if ($size <= $maxsize) {
        $sanitizer = new Sanitizer();

        $svgFile = $sanitizer->sanitize(
            file_get_contents($tmp_name)
        );

        $svgObject = simplexml_load_string($svgFile);

        $svgJson = json_encode($svgObject);

        $svgArray = json_decode($svgJson, true);

        $color = new ColorCodesCollection(
            htmlentities($_POST['color-name'], ENT_QUOTES, 'UTF-8'),
            htmlentities($_POST['color-slug'], ENT_QUOTES, 'UTF-8')
        );

        foreach ($svgArray['rect'] as $rectangle) {
            $color->addColorCode(
                $rectangle['@attributes']['fill']
            );
        }

        $colorCodesCount = $color->getColorCodesCount();
        $colorSlug = $color->getSlug();

        $colorCodes = array_map(
            function (ColorCode $colorCodeObject) {
                return $colorCodeObject->getColorHexCodeRGB();
            },
            $color->getColorCodes()
        );

        $contrastingTextColorCodes = array_map(
            function (ColorCode $colorCodeObject) {
                return $colorCodeObject->getContrastingTextColor();
            },
            $color->getColorCodes()
        );

        $contrastsSufficient = array_map(
            function (ColorCode $colorCodeObject) {
                return $colorCodeObject->isContrastSufficient();
            },
            $color->getColorCodes()
        );

        ob_start();
        require('views/partials/swatches-group.php');
        $htmlCodeOutput = htmlspecialchars(ob_get_contents());
        ob_end_clean();

        ob_start();
        require('views/partials/swatches-group-style.php');
        $cssCodeOutput = htmlspecialchars(ob_get_contents());
        ob_end_clean();

        require 'views/output.view.php';
    } else {
        $errorMessage = "File size ({$size}) exceeds maximum ({$maxsize})";

        require 'views/file-upload-error.view.php';
    }
} else {
    $errorMessage = "No file uploaded";

    require 'views/file-upload-error.view.php';
}
