<?php

require_once 'core/starter.php';

use piotrscigala\Router;
use piotrscigala\Request;

require Router::load('routes.php')
    ->direct(Request::uri());
