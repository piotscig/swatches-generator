<?php

$router->define([
    '' => 'controllers/index.php',
    'output' => 'controllers/output.php',
    'applications/swatches-generator' => 'controllers/index.php',
    'applications/swatches-generator/' => 'controllers/index.php',
    'applications/swatches-generator/output' => 'controllers/output.php',
    'applications/swatches-generator/output/' => 'controllers/output.php',
]);
